# -*- coding: utf-8 -*-
import codecs
import re
import ssfAPI_minimal as ssf
import random
import numpy as np
import cPickle
from sklearn.feature_extraction import DictVectorizer
from sklearn import tree
import argparse


class relative_resolution():
    
    def __init__(self, node,chunk):
        self.node=node
        self.chunk=chunk
        self.tree=chunk.upper
        self.file=self.tree.upper
        #self.RResolve()

    def getverbmorph(self,dchunk):
	if dchunk!=None:
		if dchunk.parent!='0' or dchunk.parent!=None:
			if not re.search(r'VGF.',dchunk.parent,re.M|re.I):
				self.getverbmorph(self.findupperchunk(dchunk.parent))
		return self.findupperchunk(dchunk.parent)

    def findupperchunk(self,vfg):
	for a in (self.chunk.upper.nodeList):
		if a.name==vfg:
			return a
	return None
    def getgender(self,nood):
	for gf in nood.nodeList:
		gfs=gf.gender
	return gfs

    def RResolve(self):
        #print 'in relative resolution'
        flagsentence=False
        flagchunk=False
        # looking into same sentence 
	verbgender=None
	selfverb=self.getverbmorph(self.chunk)
	for gf in selfverb.nodeList:
		verbgender=gf.gender
        for a in reversed(self.file.nodeList):
            if (a.name==self.tree.name or flagsentence) and not self.chunk.isPronounresolved:
                flagsentence=True
                if int(a.name)-int(self.tree.name)==0:
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if  (d.type=='DEM' and d.morphroot==u'यह' and d.upper.parentRelation=='k1' and d.upper.getAttribute('semprop')=='rest') or (d.type=='DEM' and d.morphroot==u'यह' and (d.upper.parentRelation=='k7t' or d.upper.parentRelation=='k7') and d.upper.getAttribute('semprop')=='rest') or (d.type=='DEM' and d.morphroot==u'वह' and d.upper.parentRelation=='k1' and d.upper.getAttribute('semprop')=='rest')or (d.type=='DEM' and d.morphroot==u'यह' and d.upper.parentRelation=='rt' and d.upper.getAttribute('semprop')=='rest'):
                                    if self.chunk.getAttribute('semprop')==d.upper.getAttribute('semprop'):
                                        ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(c)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    # end of JOO
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp(c,'k1u', c.upper,self.chunk.getAttribute('semprop'))
                                    if ans!=None and (self.getgender(ans)==verbgender or verbgender=='any') :
                                        if ans.isPronoun and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if c.parentRelation=='rs' and c.parent==self.chunk.name and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):
					ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(c)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break 


                    # Start of JoO
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if c.parentRelation=='pof' and re.search(r'VGF.',c.parent,re.M|re.I) and self.chunk.getAttribute('semprop')==c.getAttribute('semprop') and d.morphPOS=='n':
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(c)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break 
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp(c,'k1s', c.upper,self.chunk.getAttribute('semprop'))
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    temp_t1=False
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name:
                                    continue
                                if (c.parentRelation=='k2u' or c.parentRelation=='k1' or c.parentRelation=='k2' or c.parentRelation=='k1s') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):
                                        ans=c
                                        temp_t1=True
                    flagchunk=False
                    if temp_t1:
                        for c in reversed(a.nodeList):
                            if c.parent==ans.getAttribute('name') and c.parentRelation=='nmod' and self.chunk.getAttribute('semprop')==c.getAttribute('semprop')and c.morphroot==u'वह':
                                ans=c
                                if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(c)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            temp_t1=False
                                            break

		    flagchunk=False
                    if self.chunk.getAttribute('semprop')=='h':
                        for c in reversed(a.nodeList):
                            if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name:
                                    continue
                                if (c.parentRelation=='k1') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):
                                        ans=c
					
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break


                    flagchunk=False
                    if self.chunk.getAttribute('semprop')=='h':
                        for c in reversed(a.nodeList):
                            if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name:
                                    continue
                                if (c.parentRelation=='r6') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):
                                        ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    # end of JO
                    flagchunk=False
                    for c in reversed(a.nodeList):
                            if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name:
                                    continue
                                if (c.parentRelation=='pof') and re.search(r'VGF.',c.parent,re.M|re.I)and self.chunk.getAttribute('semprop')==c.getAttribute('semprop') and c.morphPOS=='n':
                                        ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(c)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                            if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name:
                                    continue
                                if (c.parentRelation=='vmod') and re.search(r'VGF',c.parent,re.M|re.I)and self.chunk.getAttribute('semprop')==c.getAttribute('semprop') and c.morphPOS=='n':
                                        ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(c)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    PRP_HValue=None
                    flagchunk=False
                    for e in (self.chunk.nodeList):
                        if e.type=='PRP':
                            PRP_HValue=e.lex
                    
                    chunk_no_count=0
                    flagchunk=False
                    for c in reversed(a.nodeList):
                            if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name:
                                    chunk_no_count=0
                                    continue
                                else:
                                    chunk_no_count=chunk_no_count+1
                                    for d in c.nodeList:
                                        if d.lex=='':
                                            continue
                                    if chunk_no_count>7:
                                        break
                                    if PRP_HValue =='' and (c.parentRelation=='r6') and self.chunk.number==c.number and self.chunk.getAttribute('semprop')==c.getAttribute('semprop') and c.morphPOS=='n':
                                        ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_emax_num(c,'k1', c.upper,self.chunk.getAttribute('semprop'),'person',self.chunk.number)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp(c,'k1', c.upper,self.chunk.getAttribute('semprop'))
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp(c,'r6', c.upper,self.chunk.getAttribute('semprop'))
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') : 
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp(c,'k2', c.upper,self.chunk.getAttribute('semprop'))
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if (c.parentRelation=='rt') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
				ans=c                                    
				if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
		    #print 'in ccccooooooofffff   8998'
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if (c.parentRelation=='ccof') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                ans=c    
				if True:
					#print 'in ccccooooooofffff   9'
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if (c.parentRelation=='r6-k2') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                ans=c    
				if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if (c.parentRelation=='r6') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):  
				    ans=c  
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if (c.parentRelation=='ccof') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
				    ans=c
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                #end looking into same sentence
                #start looking into -1 sentence
                if int(self.tree.name)-int(a.name)==1:
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if (c.parentRelation=='k2') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop') and self.chunk.number==c.number and self.chunk.person==c.person:
				    ans=c    
                                    if ans!=None:
                                        if self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k1', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'r6', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k2', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k4', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'ccof', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    if c.type=='VGF':
                                        ans=self.find_dep_in_stm_W_smp_per(c,'k1', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                                        
                                        
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                                if (c.parentRelation=='r6-k2') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                #end looking into -1 sentence
                #start looking into -2 sentence
                if int(self.tree.name)-int(a.name)==2:
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k1', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'r6', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k2', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k4', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                                        
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'ccof', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                
                
                for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if c.type=='VGNN'and c.parentRelation=='r6':
                                    ans=c
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
        return self.chunk                        
                                
                                
    def find_dep(self,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm(self,node,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm_W_smp(self,node,depL,stmL,semprop):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop:
                ans=a
                break
        return ans
    
    def find_dep_in_stm_W_smp_emax_num(self,node,depL,stmL,semprop,emax,number):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop and a.getAttribute('enamex_type')==emax and a.number==number:
                ans=a
                break
        return ans
    def find_dep_in_stm_W_smp_per(self,node,depL,stmL,semprop,person):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop and a.person==person:
                ans=a
                break
        return ans
    def find_dep_in_stm_W_smp_per1(self,node,depL,stmL,semprop,person):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop and (a.person==person or (person=='3h' and a.person=='3')or (person=='3' and a.person=='3h')):
                ans=a
                break
        return ans
    def find_PRP_in_stm(self,PRP_root,chunk):
        ans=None
        flag=False
        for a in reversed(chunk.upper.nodeList):
            if flag:
                for b in a.nodeList:
                    if b.morphroot in PRP_root:
                        ans=a
            if a.name==chunk.name:
                flag=True
        return ans

