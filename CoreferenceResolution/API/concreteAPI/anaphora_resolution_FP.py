# -*- coding: utf-8 -*-
import codecs
import re
import ssfAPI_minimal as ssf
import random
import numpy as np
import cPickle
from sklearn.feature_extraction import DictVectorizer
from sklearn import tree
import argparse
class firstPerson_resolution():
    
    def __init__(self, node):
        self.node=node
        self.chunk=self.node.upper
        self.tree=self.chunk.upper
        self.file=self.tree.upper
        self.verbrootforFP=[u'दोहरा',u'चाह',u'समझ',u'बता',u'बुला',u'कह']
        
    def FPResolve(self):
        #print 'in first person resolution'
	speaker=None	
	flagsentence=False
        flagchunk=False
	sprcheck=False
        for a in reversed(self.file.nodeList):
		for c in reversed(a.nodeList):
			if (a.name==self.tree.name or flagsentence) and not self.chunk.isPronounresolved:
				flagsentence=True
				if c.getAttribute('speaker')!=None:
					speaker=c.getAttribute('speaker')
					sprcheck=True
		 			break
		if sprcheck:
			break
	posn=None
	for dc in reversed(self.chunk.nodeList):
		posn=dc.getAttribute('posn')
		break
	sprcheck=False
	flagsentence=False
        flagchunk=False
	sprcheck=False
	if speaker!=None:
		for a in reversed(self.file.nodeList):
			for c in reversed(a.nodeList):
				if (int(a.name)<=int(self.tree.name) or flagsentence) and not self.chunk.isPronounresolved :
					for d in reversed(c.nodeList):
						if d.getAttribute('posn')!=None:
							if int(a.name)==int(self.tree.name) and int(d.getAttribute('posn'))< int(posn):
								if d.lex==speaker:
									ans=c
								    	if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
								        	self.chunk.PrePronounrefstm=ans.upper
								        	self.chunk.PrePronounrefnode=ans
								        	self.chunk.isPronounresolved=True
										sprcheck=True
									break
							else:
								if d.lex==speaker:
									ans=c
								    	if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
								        	self.chunk.PrePronounrefstm=ans.upper
								        	self.chunk.PrePronounrefnode=ans
								        	self.chunk.isPronounresolved=True
										sprcheck=True
									break
					if sprcheck:
						break
			if sprcheck:
				break
	flagsentence=False
        flagchunk=False
        # looking into same sentence 
        for a in reversed(self.file.nodeList):
            if (a.name==self.tree.name or flagsentence) and not self.chunk.isPronounresolved:
                flagsentence=True
                if int(a.name)-int(self.tree.name)==0:
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if c.morphroot != None:
                                if (c.morphroot in self.verbrootforFP) and (c.type=='VGF' or (c.type=='VGNN' and c.morphroot==u'कह')):
                                    ans=self.find_dep_in_stm(c,'k1', c.upper)
                                    if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                        self.chunk.PrePronounrefstm=ans.upper
                                        self.chunk.PrePronounrefnode=ans
                                        self.chunk.isPronounresolved=True
                                        break
                    if not self.chunk.isPronounresolved:
                        ans=self.find_PRP_in_stm([u'मैं',u'हम'], self.chunk)
                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                            self.chunk.PrePronounrefstm=ans.upper
                            self.chunk.PrePronounrefnode=ans
                            self.chunk.isPronounresolved=True
                            break
                    #another rule not so use full
                    #if given pronoun has pl and resolved entity has its r6 child then mark r6 as it`s referent 
                    
        #end looking into same sentence
        #start looking into -1 sentence 
        
                if int(self.tree.name)-int(a.name)==1:
                    #print 'sentence changed', a.name , self.tree.name
                    if not self.chunk.isPronounresolved:
                        ans=self.find_PRP_in_stm([u'मैं',u'हम'], self.chunk)
                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                            self.chunk.PrePronounrefstm=ans.upper
                            self.chunk.PrePronounrefnode=ans
                            self.chunk.isPronounresolved=True
                            break
                    if not self.chunk.isPronounresolved:
                        for c in reversed(a.nodeList):
                            if (self.chunk.isPronoun and not self.chunk.isPronounresolved):
                                if c.morphroot != None:
                                    if (c.morphroot in self.verbrootforFP) and (c.type=='VGF' or (c.type=='VGNN' and c.morphroot==u'कह')):
                                        ans=self.find_dep_in_stm_W_smp(c,'k1', c.upper,'h')
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    if not self.chunk.isPronounresolved:
                        for c in reversed(a.nodeList):
                            if (self.chunk.isPronoun and not self.chunk.isPronounresolved):
                                if c.morphroot != None:
                                    if (c.morphroot in self.verbrootforFP):
                                        ans=self.find_dep_in_stm_W_smp(c,'k4', c.upper,'h')
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
        #end looking into -1 sentence
        
        #start looking into -2 sentence 
        
                if int(self.tree.name)-int(a.name)==2:
                    if not self.chunk.isPronounresolved:
                        ans=self.find_PRP_in_stm([u'मैं',u'हम'], self.chunk)
                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                            self.chunk.PrePronounrefstm=ans.upper
                            self.chunk.PrePronounrefnode=ans
                            self.chunk.isPronounresolved=True
                            break
                    if not self.chunk.isPronounresolved:
                        for c in reversed(a.nodeList):
                            if (self.chunk.isPronoun and not self.chunk.isPronounresolved):
                                if c.morphroot != None:
                                    if (c.morphroot in self.verbrootforFP) and (c.type=='VGF' or (c.type=='VGNN' and c.morphroot==u'कह')):
                                        ans=self.find_dep_in_stm_W_smp(c,'k1', c.upper,'h')
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    if self.chunk.isPronounresolved:
                        if self.chunk.type=='VGNN':
                            ans=self.find_dep('r6', a)
                            if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                self.chunk.PrePronounrefstm=ans.upper
                                self.chunk.PrePronounrefnode=ans
                                self.chunk.isPronounresolved=True
                                break
        #end looking into -2 sentence
        return self.chunk                        
                                
                                
    def find_dep(self,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm(self,node,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm_W_smp(self,node,depL,stmL,semprop):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop:
                ans=a
                break
        return ans
    
    def find_PRP_in_stm(self,PRP_root,chunk):
        ans=None
        flag=False
        for a in reversed(chunk.upper.nodeList):
            if flag:
                for b in a.nodeList:
                    if b.morphroot in PRP_root:
                        ans=a
            if a.name==chunk.name:
                flag=True
        return ans
