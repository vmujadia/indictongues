# -*- coding: utf-8 -*-
import codecs
import re
import ssfAPI_minimal as ssf
import random
import numpy as np
import cPickle
from sklearn.feature_extraction import DictVectorizer
from sklearn import tree
import argparse

class reflexive_resolution():
    
    def __init__(self, node):
        self.node=node
        self.chunk=self.node.upper
        self.tree=self.chunk.upper
        self.file=self.tree.upper
        self.reflexiveList=[u'अपने',u'अपना',u'अपनी',u'खुद']
        self.reflexivePossessiveList=[u'अपने',u'अपना',u'अपनी']
        self.placeList=[u'वहां',u'यहां',u'जहां',u'वहाँ']
        #self.RFResolve()
        
    def RFResolve(self):
	posn=None
	for dc in reversed(self.chunk.nodeList):
		posn=dc.text.split('\t')[0].split('.')[0]

        if self.node.lex in self.reflexiveList:
            Ref= self.searchSibling()
            if Ref==None:
                REf1= self.searchUpwards(self.chunk)
                if REf1==None:
                    REf2=self.searchUpwards(self.chunk)

	if not self.chunk.isPronounresolved:
		#print 'reflexive pronoun not resolved'
		speaker=None	
		flagsentence=False
		flagchunk=False
		sprcheck=False
		for a in reversed(self.file.nodeList):
			for c in reversed(a.nodeList):
				if (a.name==self.tree.name or flagsentence) and not self.chunk.isPronounresolved:
					flagsentence=True
					if c.getAttribute('speaker')!=None:
						speaker=c.getAttribute('speaker')
						sprcheck=True
			 			break
			if sprcheck:
				break
		
		sprcheck=False
		flagsentence=False
		flagchunk=False
		sprcheck=False
		#print self.chunk.upper.name,self.chunk.name,speaker
		if speaker!=None:
			for a in reversed(self.file.nodeList):
				for c in reversed(a.nodeList):
					if (int(a.name)<=int(self.tree.name) or flagsentence) and not self.chunk.isPronounresolved :
						for d in reversed(c.nodeList):
							#print posn , d.getAttribute('posn'),a.name,self.tree.name
							if d.getAttribute('posn')!=None:
								if int(a.name)==int(self.tree.name) and int(d.text.split('\t')[0].split('.')[0])< int(posn):
									if d.lex==speaker:
										ans=c
									    	if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
											self.chunk.PrePronounrefstm=ans.upper
											self.chunk.PrePronounrefnode=ans
											self.chunk.isPronounresolved=True
											sprcheck=True
										break
								else:
									if d.lex==speaker:
										ans=c
									    	if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
											self.chunk.PrePronounrefstm=ans.upper
											self.chunk.PrePronounrefnode=ans
											self.chunk.isPronounresolved=True
											sprcheck=True
										break
						if sprcheck:
							break
				if sprcheck:
					break
        return self.chunk
                
    def searchUpwards(self,currentnode):
	posn=None
	for dc in reversed(self.chunk.nodeList):
		posn=dc.text.split('\t')[0].split('.')[0]
        if currentnode!=None:
	    #print currentnode.parent,currentnode.name
            if currentnode.parent!='0':
                if not self.isRefkexivepossesive(self.node.morphroot) and currentnode.parent!='0':
                    if self.checkReflexiveReferents(self.chunknametochunkrefmapping(currentnode.parent)):
                        #print 'return 1',self.chunknametochunkrefmapping(currentnode.parent).name
                        if not self.chunk.isPronounresolved:
                                self.chunk.PrePronounrefstm=self.chunknametochunkrefmapping(currentnode.parent).upper
                                self.chunk.PrePronounrefnode=self.chunknametochunkrefmapping(currentnode.parent)
                                self.chunk.isPronounresolved=True
                        return self.chunknametochunkrefmapping(currentnode.parent)
                    
                for c in currentnode.upper.nodeList:
                    if c.parent==currentnode.parent and currentnode.name!=c.name:
                        #print c.name , c.getAttribute('head') , 'in search upword'
                        if self.checkReflexiveReferents(c):
                            if c.type=='CCP':
                                for c1 in c.upper.nodeList:
                                    if c1.parent==c.parent and c.name!=c1.name:
                                        if c1.parentRelation=='ccof':
                                            #print 'return 2',c1.name
                                            if not self.chunk.isPronounresolved:
                                                self.chunk.PrePronounrefstm=c1.upper
                                                self.chunk.PrePronounrefnode=c1
                                                self.chunk.isPronounresolved=True
                                            return c1
                            #print 'return 3',c.name
			    for d in reversed(c.nodeList):
				posnd=d.text.split('\t')[0].split('.')[0]
				
                            if not self.chunk.isPronounresolved and int(posnd)< int(posn):
                                self.chunk.PrePronounrefstm=c.upper
                                self.chunk.PrePronounrefnode=c
                                self.chunk.isPronounresolved=True
                            return c
                
                
                self.searchUpwards(self.chunknametochunkrefmapping(currentnode.parent))    
            
        
        #if currentnode.parent=='0':
        #    return None
        #else:
        #    self.searchUpwards(self.chunknametochunkrefmapping(currentnode.parent))
        
    def checkReflexiveReferents(self,currentnode):
        ref=False
	for d in reversed(currentnode.nodeList):
		posnd=d.text.split('\t')[0].split('.')[0]
	
	for dc in reversed(self.chunk.nodeList):
		posn=dc.text.split('\t')[0].split('.')[0]
	#print '\n\n\n\n\nVandan\n\n\n\n\n'
        if currentnode.parentRelation=='rad' or currentnode.parentRelation=='k1' or currentnode.parentRelation=='k4'or (currentnode.parentRelation=='k2' and currentnode.getAttribute('semprop')=='h') and int(posnd)< int(posn):
            ref= True
        return ref
        
    def chunknametochunkrefmapping(self,parent):
        #print 'in chunknametochunkrefmapping'
        for c in self.chunk.upper.nodeList:
            if c.name==parent:
                return c
    
    def searchSibling(self):
        for c in self.chunk.upper.nodeList:
	    #print c.parent,self.chunk.parent,'VANDAN MUJADIA'
            if c.parent==self.chunk.parent and self.chunk.name!=c.name:
                if c.parentRelation=='r6':
                    self.chunk.PrePronounrefstm=c.upper
                    self.chunk.PrePronounrefnode=c
                    self.chunk.isPronounresolved=True
                    #print 'return 0',c.name
                    return c
        return None
        
            
        
    def isresolvable(self):
        print 'in is resolvable'
        
    def isreflexive(self):
        print 'in is reflexive'
        print 'check in reflexiveList'

    def isRefkexivepossesive(self,word):
        if word in self.reflexivePossessiveList:
            return True
        else:
            return False
        

