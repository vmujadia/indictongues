# -*- coding: utf-8 -*-
import re
import ssfAPI_minimal as ssf
import codecs
import argparse


class markSpeakerSsf_main():
	def __init__(self, filepointer):
		self.filepointer=filepointer
		self.possiblespeakerList=['narrator']
		self.speaker=None
		self.previousspeaker=None
		self.verbList=[u'दोहरा',u'चाह',u'बता',u'बुला',u'कह',u'बोल']
		self.notinverbList=[u'कहिये']
		self.secondpresonpronoun_root__list=[]
		self.firstpersonpronoun_root_list=[u'मैं']
		self.thirdpersonpronoun_root__list=[]
		self.refelexivepronoun_root_list=[]
		#self.markSpeakerSsf_run()
		

	def findk1forverb(self,node,chunkname):
		for b in node.upper.upper.nodeList:
			if b.parentRelation=='k1' and chunkname==b.parent:					
				for c in b.nodeList:
					if (c.type=='NN' or c.type=='NNP' or c.type=='NNPC' or c.type=='NNC' or c.type=='PRP'):
						self.previousspeaker=self.speaker
						self.speaker=c.lex
						return True
					#elif (c.type=='PRP' and c.lex not in self.firstpersonpronoun_root_list):
					#	self.previousspeaker=self.speaker
					#	self.speaker='narrator'
					#	return True
							
		return False


	def markSpeakerSsf_run(self):
		newFileList = []
		anaphoraDic={}
		flag=False
		sprdrel=False
		sprdrel1=False
		trcount=1
		if True:
		    d = self.filepointer
		    for tree in d.nodeList :
			speakercount=0
			tokencount=0
		        for chunkNode in tree.nodeList :
			    if tree.name=='1' and trcount==1:
				trcount+=1
			    if sprdrel1:
					    if True:
						if self.speaker!=None:
					    		chunkNode.addAttribute('speaker',self.speaker)
						#else:
						#	chunkNode.addAttribute('speaker','narrator')
						if self.speaker not in self.possiblespeakerList:
							self.possiblespeakerList.append(self.speaker)
						sprdrel1=False
	
			    if sprdrel:
				    speakercount=speakercount+1
				    if speakercount==2:
					if self.speaker!=None:
				    		chunkNode.addAttribute('speaker',self.speaker)
					#else:
					#	chunkNode.addAttribute('speaker','narrator')
					if self.speaker not in self.possiblespeakerList:
						self.possiblespeakerList.append(self.speaker)
				    	sprdrel=False
			    
		            

			    if chunkNode.parentRelation=='spr':
				    sprdrel=True
				    	
			    onlyonespr=True
		            for node in chunkNode.nodeList:
				    if node.type=='NNP' or node.type=='NNPC':
            				    if node.lex not in self.possiblespeakerList:
					    	self.possiblespeakerList.append(node.lex)


				    if (node.type=='NNP' or node.type=='NNPC' or (node.type=='NN' and onlyonespr)) and sprdrel:
					    self.previousspeaker=self.speaker
					    self.speaker=node.lex
					    onlyonespr=False	
					    if node.lex not in self.possiblespeakerList:
					    	self.possiblespeakerList.append(node.lex)
				    if node.morphroot in self.verbList and node.type=='VM' and chunkNode.parentRelation!='spn' and node.name not in self.notinverbList:
					    if self.findk1forverb(node,node.upper.name):
					    	sprdrel1=True

			#print tree.printSSFValue(allFeat=False)
		return d


'''
'resolution main class'
if __name__ == '__main__' :
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', '-please specify the input file path')
    args = parser.parse_args()
    if args.input==None:
        print 'For help please use: python anaphora_resolution_main.py -h'
        exit()
    #try:
    if True:
        first=markSpeakerSsf_main(args.input)
    #except:
        #print 'something went wrong please check the SSF file format'
'''
