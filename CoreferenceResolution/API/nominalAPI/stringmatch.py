# -*- coding: utf-8 -*-
import copy
import collections
import codecs
import re
import ssfAPI_minimal as ssf
import random
import numpy as np
import cPickle

class stringmatch():
    
    	def __init__(self):
		fdf='init'

	def dumy_string_match(self,final_list_entity):
	    count = 0
	    coref_dic={}
	    for i in final_list_entity :
		flag = False
		if not flag:
		    coref_dic[count]={}
		    coref_dic[count][1]=i
		    count = count + 1
		else:
		    coref_dic[index][len(coref_dic[index])+1]=i
	    return coref_dic


	def get_matched_index_2(self,coref_dic,value,chainid):
	    for i in coref_dic:
		try:
		    if chainid !=i:
		        for j in coref_dic[i]:
		            if ' '.join(val.morphroot for val in coref_dic[i][j]) in ' '.join(v.morphroot for v in value) or ' '.join(v.morphroot for v in value) in ' '.join(value.morphroot for value in coref_dic[i][j]):
		                if len(coref_dic[i][j])>1:
		                    if ' '.join(val.morphroot for val in coref_dic[i][j]).strip() in name_dic:
		                        return True , i
		                if len(value)>1:
		                    if ' '.join(v.morphroot for v in value).strip() in name_dic:
		                        return True , i
		except:
		    pass

	    return False , 0


	def get_matched_index_for_string_match_constrain(self,coref_dic,value,chainid):
	    for i in coref_dic:
		if chainid != i:
		    for j in coref_dic[i]:
		        if  coref_dic[i][j][len(coref_dic[i][j])-1].morphroot!= None  and value[len(value)-1].morphroot != None:
		            if (coref_dic[i][j][len(coref_dic[i][j])-1].morphroot).strip() == (value[len(value)-1].morphroot).strip():
		                if (' '.join(fg.morphroot for fg in coref_dic[i][j]) in ' '.join(fg1.morphroot for fg1 in value)) or (' '.join(fg.morphroot for fg in value) in ' '.join(fg1.morphroot for fg1 in coref_dic[i][j])) or (' '.join(fg.morphroot for fg in value) == ' '.join(fg1.morphroot for fg1 in coref_dic[i][j])):
		                    if (len(coref_dic[chainid])==1 or len(coref_dic[i])==1) and (coref_dic[i][j][len(coref_dic[i][j])-1].number==value[len(value)-1].number):
		                        return True , i
		                    else:
		                        cmp1=''

		                        for j1 in coref_dic[chainid]:
		                            for j2 in coref_dic[chainid][j1]:
		                                if j2.type in ['NNPC']:
		                                    cmp1=coref_dic[chainid][j1]
		                                    break
		                        if cmp1=='':
		                            for j1 in coref_dic[chainid]:
		                                for j2 in coref_dic[chainid][j1]:
		                                    if j2.type in ['NNP','NNPC']:
		                                        cmp1=coref_dic[chainid][j1]
		                                        break

		                        cmp2=''

		                        for j1 in coref_dic[i]:
		                            for j2 in coref_dic[i][j1]:
		                                if j2.type in ['NNPC']:
		                                    cmp2=coref_dic[i][j1]
		                                    break

		                        if cmp2=='':
		                            for j1 in coref_dic[i]:
		                                for j2 in coref_dic[i][j1]:
		                                    if j2.type in ['NNP','NNPC']:
		                                        cmp2=coref_dic[i][j1]
		                                        break

		                        if ' '.join(f.lex for f in cmp1)==' '.join(f.lex for f in cmp2) or ' '.join(f.lex for f in cmp2) == ' '.join(f.lex for f in value) or  ' '.join(f.lex for f in cmp1)==' '.join(f.lex for f in coref_dic[i][j]):
		                            return True,i
	    return False , 0


	def string_match_other_WE(self,coref_dic):
	    fcoref_dic123=copy.deepcopy(coref_dic)
	    count = 1
	    for i in fcoref_dic123:
		for j in fcoref_dic123[i]:
		    flag=False
		    if True:
		        flag , index = self.get_matched_index_for_string_match_constrain(coref_dic,fcoref_dic123[i][j],i)
		        if not flag:
		            continue
		        else:
		            for k in coref_dic[i]:
		                coref_dic[index][len(coref_dic[index])+1]=coref_dic[i][k]
		            coref_dic[i]=[]

	    return coref_dic



	def get_matched_index_for_string_match(self,coref_dic,value,chainid):
	    for i in coref_dic:
		if chainid != i:
		    for j in coref_dic[i]:
		        if coref_dic[i][j][len(coref_dic[i][j])-1].morphroot!= None  and value[len(value)-1].morphroot != None:
		            if (coref_dic[i][j][len(coref_dic[i][j])-1].morphroot).strip() == (value[len(value)-1].morphroot).strip():
		                if (' '.join(fg.morphroot for fg in coref_dic[i][j]) in ' '.join(fg1.morphroot for fg1 in value)) or (' '.join(fg.morphroot for fg in value) in ' '.join(fg1.morphroot for fg1 in coref_dic[i][j])) or (' '.join(fg.morphroot for fg in value) == ' '.join(fg1.morphroot for fg1 in coref_dic[i][j])):
		                    if (coref_dic[i][j][len(coref_dic[i][j])-1].parent.lex==value[len(value)-1].lex) or (coref_dic[i][j][len(coref_dic[i][j])-1].lex==value[len(value)-1].parent.lex):
		                        return True , i
	    return False , 0



	def string_match_NNP(self,coref_dic):
	    fcoref_dic123=copy.deepcopy(coref_dic)
	    count = 1
	    for i in fcoref_dic123:
		for j in fcoref_dic123[i]:
		    flag=False
		    if fcoref_dic123[i][j][len(fcoref_dic123[i][j])-1].type=='NNP':
		        flag , index = self.get_matched_index_for_string_match(coref_dic,fcoref_dic123[i][j],i)
		        if not flag:
		            continue
		        else:
		            for k in coref_dic[i]:
		                coref_dic[index][len(coref_dic[index])+1]=coref_dic[i][k]
		            coref_dic[i]=[]

	    return coref_dic



	def get_matched_index_for_string_match_constrain_adj(self,coref_dic,value,chainid):
	    for i in coref_dic:
		if chainid != i:
		    for j in coref_dic[i]:
		        if  coref_dic[i][j][len(coref_dic[i][j])-1].morphroot!= None  and value[len(value)-1].morphroot != None:
		            if (coref_dic[i][j][len(coref_dic[i][j])-1].morphroot).strip() == (value[len(value)-1].morphroot).strip():
		                if coref_dic[i][j][len(coref_dic[i][j])-1].type in ['NNP','NNPC'] or value[len(value)-1].type in ['NNP','NNPC'] :
		                    if (len(coref_dic[chainid])==1 and len(coref_dic[i])==1):
		                        return True,i
	    return False , 0


	def string_match_other_adj(self,coref_dic):
	    fcoref_dic123=copy.deepcopy(coref_dic)
	    count = 1
	    for i in fcoref_dic123:
		for j in fcoref_dic123[i]:
		    flag=False
		    if True:
		        flag , index = self.get_matched_index_for_string_match_constrain_adj(coref_dic,fcoref_dic123[i][j],i)
		        if not flag:
		            continue
		        else:
		            for k in coref_dic[i]:
		                coref_dic[index][len(coref_dic[index])+1]=coref_dic[i][k]
		            coref_dic[i]=[]

	    return coref_dic
