import copy
import collections
import codecs
import re
import ssfAPI_minimal as ssf
import random
import numpy as np
import cPickle

class match_firstname():
    
    	def __init__(self):
		fdf='init'

	def get_matched_index_first_name (self,coref_dic,value,index):
	    for i in coref_dic:
		if index!=i:
			for j in coref_dic[i]:
		    		if coref_dic[i][j][len(coref_dic[i][j])-1].morphroot!= None  and value[len(value)-1].morphroot != None:
		        		#if (coref_dic[i][j][len(coref_dic[i][j])-1].morphroot).strip() == (value[len(value)-1].morphroot).strip():
			    		#print 'VANDAN MUJADIAIIII',' '.join(fg.morphroot for fg in coref_dic[i][j]) ,' '.join(fg1.morphroot for fg1 in value)
		            		if (' '.join(fg.morphroot for fg in coref_dic[i][j]) in ' '.join(fg1.morphroot for fg1 in value)) or (' '.join(fg.morphroot for fg in value) in ' '.join(fg1.morphroot for fg1 in coref_dic[i][j])) or (' '.join(fg.morphroot for fg in value) == ' '.join(fg1.morphroot for fg1 in coref_dic[i][j])):
						if len(coref_dic[i])==1 or len(coref_dic[index])==1:
		                			return True , i
	    return False , 0

	def match_for_first_name(self,coref_dic):
		fcoref_dic123=copy.deepcopy(coref_dic)
		count = 1
		for i in fcoref_dic123:
			for j in fcoref_dic123[i]:				
					flag , index = self.get_matched_index_first_name(coref_dic,fcoref_dic123[i][j],i)
					if not flag:
						continue
					else:
						for k in fcoref_dic123[i]:
							coref_dic[index][len(coref_dic[index])+1]=fcoref_dic123[i][k]
							if k in coref_dic[i]:
								coref_dic[i].pop(k)
		return coref_dic
