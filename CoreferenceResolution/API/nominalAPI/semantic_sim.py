# -*- coding: utf-8 -*-
import collections
import codecs
import re
import ssfAPI_minimal as ssf
import random
import numpy as np
import cPickle
import socket
import cPickle
import copy

class semantic_sim():
    
    	def __init__(self):
		self.fdf='init'

	def get_matched_index_for_word2vec(self,coref_dic,value,chainid):
	    for i in coref_dic:
		if chainid !=i and i>chainid:
		    for j in coref_dic[i]:
		        if coref_dic[i][j][len(coref_dic[i][j])-1].morphroot!=None and value[len(value)-1].morphroot != None:
		            sumi=0
		            count=1
		            for c in coref_dic[chainid]:
		                for d in coref_dic[chainid][c]:
		                    for e in coref_dic[i][j]:
		                        count=count+1
		                        clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		                        clientsocket.connect(('10.3.1.91', 8036))
		                        clientsocket.send(coref_dic[i][j][len(coref_dic[i][j])-1].lex+':'+value[len(value)-1].lex) # server input
		                        data = clientsocket.recv(99999)
		                        data_arr = cPickle.loads(data)
		                        clientsocket.close()
		                        sumi=float(data_arr)+sumi
		            if count!=0:
		                if (sumi/count)>0.50 and value[len(value)-1].gender == coref_dic[i][j][len(coref_dic[i][j])-1].gender:
		                    return True , i
	    return False , 0

	def word2vec_match(self,coref_dic):
	    fcoref_dic123=copy.deepcopy(coref_dic)
	    count = 1
	    for i in fcoref_dic123:
		for j in fcoref_dic123[i]:
		    flag=False
		    if True:
		        flag , index = self.get_matched_index_for_word2vec(coref_dic,fcoref_dic123[i][j],i)
		        if not flag:
		            continue
		        else:
		            for k in coref_dic[i]:
		                coref_dic[index][len(coref_dic[index])+1]=coref_dic[i][k]
		            coref_dic[i]=[]

	    return coref_dic
