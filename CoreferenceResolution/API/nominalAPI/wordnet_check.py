# -*- coding: utf-8 -*-
import copy
import collections
import codecs
import re
import ssfAPI_minimal as ssf
import random
import numpy as np
import cPickle
import socket
import cPickle


class wordnet_check():
    
    	def __init__(self):
		fdf='init'

	def get_matched_for_wordnet(self,coref_dic,value,chainid):
	    for i in coref_dic:
		if chainid != i:
		    for j in coref_dic[i]:
		        if  coref_dic[i][j][len(coref_dic[i][j])-1].morphroot!= None  and value[len(value)-1].morphroot != None:
		            if (coref_dic[i][j][len(coref_dic[i][j])-1].morphroot).strip() == (value[len(value)-1].morphroot).strip():
		                cmp1=''
		                for j1 in coref_dic[chainid]:
		                    for j2 in coref_dic[chainid][j1]:
		                        if j2.type in ['NNPC']:
		                            cmp1=coref_dic[chainid][j1]
		                            break
		                if cmp1=='':
		                    for j1 in coref_dic[chainid]:
		                        for j2 in coref_dic[chainid][j1]:
		                            if j2.type in ['NNP','NNPC']:
		                                cmp1=coref_dic[chainid][j1]
		                                break
		                cmp2=''
		                for j1 in coref_dic[i]:
		                    for j2 in coref_dic[i][j1]:
		                        if j2.type in ['NNPC']:
		                            cmp2=coref_dic[i][j1]
		                            break

		                if cmp2=='':
		                    for j1 in coref_dic[i]:
		                        for j2 in coref_dic[i][j1]:
		                            if j2.type in ['NNP','NNPC']:
		                                cmp2=coref_dic[i][j1]
		                                break

		                if cmp1!='' and cmp2!='':
		                    clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		                    clientsocket.connect(('10.3.1.91', 8036))
		                    clientsocket.send(cmp1[len(cmp1)-1].morphroot+":"+cmp2[len(cmp2)-1].morphroot) #server input
		                    data = clientsocket.recv(307200)
		                    data_arr = cPickle.loads(data)
		                    #print ' '.join(f.lex for f in cmp1),' '.join(f.lex for f in cmp2),data_arr
		                    if data_arr=='True':
		                        return True,i
		                elif len(coref_dic[i])==1 and len(value)==1:
		                    clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		                    clientsocket.connect(('10.3.1.91', 8036))
		                    clientsocket.send(coref_dic[i][j][len(coref_dic[i][j])-1].morphroot+":"+value[len(value)-1].morphroot) #server input
		                    data = clientsocket.recv(307200)
		                    data_arr = cPickle.loads(data)
		                    #print 'elif part',' '.join(f.lex for f in coref_dic[i][j]),' '.join(f.lex for f in value),data_arr
		                    if data_arr=='True':
		                        return True,i
	    return False , 0


	def string_match_wordnet(self,coref_dic):
	    fcoref_dic123=copy.deepcopy(coref_dic)
	    count = 1
	    for i in fcoref_dic123:
		for j in fcoref_dic123[i]:
		    flag=False
		    if True:
		        flag , index = self.get_matched_for_wordnet(coref_dic,fcoref_dic123[i][j],i)
		        if not flag:
		            continue
		        else:
		            for k in coref_dic[i]:
		                coref_dic[index][len(coref_dic[index])+1]=coref_dic[i][k]
		            coref_dic[i]=[]
	    return coref_dic

