# -*- coding: utf-8 -*-
import sys
import imp
import ssfAPI_minimal as ssf
import random
import numpy as np
import cPickle
from sklearn.feature_extraction import DictVectorizer
from sklearn import tree
from sklearn.externals import joblib
from sklearn import tree
import argparse
import pickle
reload(sys)
sys.setdefaultencoding("utf-8")

reportingverb=[u'कह',u'बता']

def folderWalk(folderPath):
    import os
    fileList = []
    for dirPath , dirNames , fileNames in os.walk(folderPath) :
        for fileName in fileNames :
            fileList.append(os.path.join(dirPath , fileName))
    return fileList

def checkforreportingverb(tree):
	for chunknode in tree.nodeList:
		for node in chunknode.nodeList:
			if node.morphroot in reportingverb:
				if node.getAttribute('posn')!=None:
					return True,int(node.getAttribute('posn'))
	return False,0
def getlastverb(doc,node):
	send = ''
	for tree in doc.nodeList:
		if int(node.upper.upper.name)-1 == int(tree.name):
			for chunknode in tree.nodeList:
				for node1 in chunknode.nodeList:
					if node1.type=='VM':
						send=node1
			return send
def getsamelastverb(doc,node):
	send = ''
	for tree in doc.nodeList:
		if int(node.upper.upper.name) == int(tree.name):
			for chunknode in tree.nodeList:
				for node1 in chunknode.nodeList:
					if node1.type=='VM':
						send=node1
			return send

def getsamelastverbbeforepronoun(doc,node):
	send = ''
	for tree in doc.nodeList:
		if int(node.upper.upper.name) == int(tree.name):
			for chunknode in tree.nodeList:
				for node1 in chunknode.nodeList:
					if node1.getAttribute('posn')!=None or node1.getAttribute('posn')!='':
						if node1.morphroot not in reportingverb and  node1.type=='VM' and int(node1.getAttribute('posn'))<int(node.getAttribute('posn')):
							send=node1
			return send


def findkicc(doc,node):
	send = ''
	for tree in doc.nodeList:
		if int(node.upper.upper.name) == int(tree.name):
			for chunknode in tree.nodeList:
				for node1 in chunknode.nodeList:
					if node1.type=='CC' and node1.lex==u'कि':
						send=node1
			return send


def resolveesa(doc,node,fname):
	if node.getAttribute('posn')=='10':
		referent = getlastverb(doc,node)
		refsen,refchunk = referent.upper.upper.name,referent.upper.name
	else:		
		for tree in doc.nodeList:
			if int(node.upper.upper.name)==int(tree.name) :
				flag_reporting_verb,posn_reporting_verb = checkforreportingverb(tree)
				if flag_reporting_verb:
					if node.getAttribute('posn')!=None:
						posn_node = int(node.getAttribute('posn'))
						if posn_reporting_verb>posn_node:
							#NoCASEFOUNDYET
							pass
							#print posn_reporting_verb-posn_node,'11111111'
						else:
							ccchunk=findkicc(doc,node)
							if ccchunk.getAttribute('posn')!=None and ccchunk.getAttribute('posn')!='':
								posn_ki = int(findkicc(doc,node).getAttribute('posn'))
							else:
								posn_ki = 100000000
							if posn_ki > posn_node and posn_reporting_verb<posn_ki:
								referent = getsamelastverb(doc,node)
								refsen,refchunk = referent.upper.upper.name,referent.upper.name
								#print refsen,refchunk
							else:
								if posn_node<100:
									referent = getlastverb(doc,node)
									refsen,refchunk = referent.upper.upper.name,referent.upper.name
									#print refsen,refchunk
								else:
									referent=getsamelastverbbeforepronoun(doc,node)
									refsen,refchunk = referent.upper.upper.name,referent.upper.name
									#print refsen,refchunk
									#print node.text.strip(),fname
									#print posn_node,posn_reporting_verb,'2222222222222222',findkicc(doc,node).getAttribute('posn')
				else:
					posn_node=int(node.getAttribute('posn'))
					print '\n\n\n\n\n',node.text.strip(),fname		
					if posn_node>100:
						referent=getsamelastverbbeforepronoun(doc,node)
						if referent==None  or referent=='':
							pass
							#print 'None'
							#referent not found
						else:
							#print referent
							refsen,refchunk = referent.upper.upper.name,referent.upper.name
							#print refsen,refchunk
					else:
						referent = getlastverb(doc,node)
						refsen,refchunk = referent.upper.upper.name,referent.upper.name
						#print '=================>',refsen,refchunk
if __name__ == '__main__' :
	inputPath = sys.argv[1]
    	fileList = folderWalk(inputPath)
	newFileList=fileList
    	#newFileList = []
	#newFileList.append(inputPath)
	for fileName in newFileList :
		#print 'Name ::: ',fileName
		d = ssf.Document(fileName)
		for tree in d.nodeList :
		    for chunkNode in tree.nodeList :
		        for node in chunkNode.nodeList:
				if node.type=='PRP' and node.getAttribute('reftype')=='E':
					ref = node.getAttribute('ref') 
					if ',' in ref:
						ref=ref.split(',')
					if '%' in ref:
						rsen=ref.split('%')[1]
						rchunk=ref.split('%')[2]
					else:
						rsen=tree.name
						rchunk=ref
					if node.morphroot.strip()==u'ऐसा':
						resolveesa(d,node,fileName)

