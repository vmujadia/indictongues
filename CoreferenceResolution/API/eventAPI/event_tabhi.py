# -*- coding: utf-8 -*-
import sys
import imp
import ssfAPI_minimal as ssf
import random
import numpy as np
import cPickle
from sklearn.feature_extraction import DictVectorizer
from sklearn import tree
from sklearn.externals import joblib
from sklearn import tree
import argparse
import pickle
reload(sys)
sys.setdefaultencoding("utf-8")

reportingverb=[u'कह',u'बता']
jabpronounpairs=[u'जब']

def checkforotherpronounpairM(tree,nodep):
	for chunknode in tree.nodeList:
		for node in chunknode.nodeList:
                        nodepd=nodep.getAttribute('posn')
                        if nodepd==None:
                                nodepd='0'
                        noded=node.getAttribute('posn')
                        if noded==None:
                                noded='0'
			if node.morphroot in jabpronounpairs:
				if int(noded)<=int(nodepd):
					if node.getAttribute('posn')!=None:
						return True,True,node
				else:
					if node.getAttribute('posn')!=None:
						return True,False,node
	return False,False,0			

def getlastverb(doc,node):
	send = ''
	for tree in doc.nodeList:
		if int(node.upper.upper.name)-1 == int(tree.name):
			for chunknode in tree.nodeList:
				for node1 in chunknode.nodeList:
					if node1.type=='VM':
						send=node1
			return send



def folderWalk(folderPath):
    import os
    fileList = []
    for dirPath , dirNames , fileNames in os.walk(folderPath) :
        for fileName in fileNames :
            fileList.append(os.path.join(dirPath , fileName))
    return fileList

def findchunkfromname(node,parentname):
	for i in node.upper.nodeList:
		if i.name==parentname:
			return i

def checkforreportingverb(tree):
	for chunknode in tree.nodeList:
		for node in chunknode.nodeList:
			if node.morphroot in reportingverb:
				if node.getAttribute('posn')!=None:
					return True,int(node.getAttribute('posn'))
	return False,0

def checkforotherpronounpair(tree,nodep):
	for chunknode in tree.nodeList:
		for node in chunknode.nodeList:
                        nodepd=nodep.getAttribute('posn')
                        if nodepd==None:
                                nodepd='0'
                        noded=node.getAttribute('posn')
                        if noded==None:
                                noded='0'
			if node.morphroot in jabpronounpairs and int(noded)>=int(nodepd):
				if node.getAttribute('posn')!=None:
					return True,node
	return False,0			

def findkicc(doc,node):
	send = ''
	for tree in doc.nodeList:
		if int(node.upper.upper.name) == int(tree.name):
			for chunknode in tree.nodeList:
				for node1 in chunknode.nodeList:
					if node1.type=='CC' and node1.lex==u'कि':
						send=node1
			return send

def getsamelastverbafterpronoun(doc,node):
	send = ''
	for tree in doc.nodeList:
		if int(node.upper.upper.name) == int(tree.name):
			for chunknode in tree.nodeList:
				for node1 in chunknode.nodeList:
					if node1.getAttribute('posn')!=None or node1.getAttribute('posn')!='':
						dis1=node1.getAttribute('posn')
						dis=node.getAttribute('posn')
						if dis1==None:
							dis1='0'
						if dis==None:
							dis='0'
						#print 'VGF' in node1.upper.name.strip(),node1.upper.name.strip(),'======-'
						#if node1.morphroot not in reportingverb and  node1.type=='VM' and 'VGF' in node1.upper.name.strip() and int(dis1)>=int(dis):
						if node1.type=='VM' and 'VGF' in node1.upper.name.strip() and int(dis1)>=int(dis):
							return node1
			return send

def getverbotherthenreportingbeforepair(doc,node,pair):
	send = ''
	for tree in doc.nodeList:
		if int(node.upper.upper.name) == int(tree.name):
			for chunknode in tree.nodeList:
				for node1 in chunknode.nodeList:
					if node1.getAttribute('posn')!=None or node1.getAttribute('posn')!='':
						dis1=node1.getAttribute('posn')
						dis=node.getAttribute('posn')
						disp=pair.getAttribute('posn')
						if dis1==None:
							dis1='0'
						if dis==None:
							dis='0'
						if disp==None:
							disp='0'
						if  node1.type=='VM' and 'VGF' in node1.upper.name.strip() and int(dis1)>=int(dis) and int(disp)>int(dis1):
							return node1
			return send
def getsamelastverbbeforepronoun(doc,node):
	send = ''
	for tree in doc.nodeList:
		if int(node.upper.upper.name) == int(tree.name):
			for chunknode in tree.nodeList:
				for node1 in chunknode.nodeList:
					if node1.getAttribute('posn')!=None or node1.getAttribute('posn')!='':
						dis1=node1.getAttribute('posn')
						dis=node.getAttribute('posn')
						if dis1==None:
							dis1='0'
						if dis==None:
							dis='0'
						if  node1.type=='VM' and 'VGF' in node1.upper.name.strip() and int(dis1)<=int(dis):
                                                    send=node1	
                                                #if  node1.type=='VM' and int(node1.getAttribute('posn'))<int(node.getAttribute('posn')):
						#	send=node1
			return send


def findkicc(doc,node):
	send = ''
	for tree in doc.nodeList:
		if int(node.upper.upper.name) == int(tree.name):
			for chunknode in tree.nodeList:
				for node1 in chunknode.nodeList:
					if node1.type=='CC' and node1.lex==u'कि':
						send=node1
			return send


def resolve(doc,node,fname):
    #print node.text.strip()
    posnn=node.getAttribute('posn')
    if posnn!=None:
        posnn=int(posnn)
    else:
        posnn=0
    if posnn<=40:
        referent=getlastverb(doc,node)
        refsen,refchunk = referent.upper.upper.name,referent.upper.name
	return referent
        #print '-1',refsen,refchunk,node.upper.upper.name
    else:
        for tree in doc.nodeList:
		if int(node.upper.upper.name)==int(tree.name) :
			pyn,p2yn,pair=checkforotherpronounpairM(tree,node)
                        if not pyn:
                            referent=getsamelastverbbeforepronoun(doc,node)
                            if referent!='':
                                refsen,refchunk = referent.upper.upper.name,referent.upper.name
				return referent
			        #print '0',refsen,refchunk,node.upper.upper.name
                        else:
                                        #print 'one'
					if p2yn:
                                                #print 'two'
						referent=getsamelastverbbeforepronoun(doc,node)
						refsen,refchunk = referent.upper.upper.name,referent.upper.name
						return referent
						#print refsen,refchunk,node.upper.upper.name
					else:
                                                #print 'three'
						flag_reporting_verb,posn_reporting_verb = checkforreportingverb(tree)
						if flag_reporting_verb:
							#print 'first part'
							if node.getAttribute('posn')!=None:
								posn_node = int(node.getAttribute('posn'))
								if posn_reporting_verb>posn_node:
									pyn,pair=checkforotherpronounpair(tree,node)
									if pyn:
										referent = getverbotherthenreportingbeforepair(doc,node,pair)
										if referent!='':
											refsen,refchunk = referent.upper.upper.name,referent.upper.name
											return referent
											#print "==1==",refsen,refchunk,node.upper.upper.name
									else:
										referent = getsamelastverbafterpronoun(doc,node)
										if referent!='':
											refsen,refchunk = referent.upper.upper.name,referent.upper.name
											return referent
											#print '==2==',refsen,refchunk,node.upper.upper.name
								else:
				                	                #print 'first part-2'
									ccchunk=findkicc(doc,node)
									if ccchunk.getAttribute('posn')!=None and ccchunk.getAttribute('posn')!='':
										posn_ki = int(findkicc(doc,node).getAttribute('posn'))
									else:
										posn_ki = 100000000
									if posn_ki > posn_node and posn_reporting_verb<posn_ki:
				                    	                        #print 'first part-3'
										pyn,pair=checkforotherpronounpair(tree,node)
										if pyn:
				                        	                        #print 'first part-3-1'
											referent = getverbotherthenreportingbeforepair(doc,node,pair)
											if referent!='':
												refsen,refchunk = referent.upper.upper.name,referent.upper.name
												return referent					
												#print "==3==",refsen,refchunk,node.upper.upper.name
										else:
				                        	                        #print 'first part-3-2'
											referent = getsamelastverbafterpronoun(doc,node)
											if referent!='':
												refsen,refchunk = referent.upper.upper.name,referent.upper.name
												return referent
												#print '==4==',refsen,refchunk,node.upper.upper.name
									else:
				                    	                        #print 'first part-4'
										pyn,pair=checkforotherpronounpair(tree,node)
										if pyn:
											referent = getverbotherthenreportingbeforepair(doc,node,pair)
											if referent!='':
												refsen,refchunk = referent.upper.upper.name,referent.upper.name
												return referent
												#print "==5==",refsen,refchunk,node.upper.upper.name
										else:
											referent = getsamelastverbafterpronoun(doc,node)
											if referent!='':
												refsen,refchunk = referent.upper.upper.name,referent.upper.name
												return referent
												#print '==6==',refsen,refchunk,node.upper.upper.name
						else:
							#print 'second part'
							pyn,pair=checkforotherpronounpair(tree,node)
							if pyn:
								referent = getverbotherthenreportingbeforepair(doc,node,pair)
								if referent!='':
									refsen,refchunk = referent.upper.upper.name,referent.upper.name
									return referent
									#print "==7==",refsen,refchunk,node.upper.upper.name
							else:
								referent = getsamelastverbafterpronoun(doc,node)
								if referent!='':
									refsen,refchunk = referent.upper.upper.name,referent.upper.name
									return referent
									#print '==8==',refsen,refchunk,node.upper.upper.name
	

if __name__ == '__main__' :
	inputPath = sys.argv[1]
    	fileList = folderWalk(inputPath)
	newFileList=fileList
    	#newFileList = []
	#newFileList.append(inputPath)
	for fileName in newFileList :
		print 'Name ::: ',fileName
		d = ssf.Document(fileName)
		for tree in d.nodeList :
		    for chunkNode in tree.nodeList :
		        for node in chunkNode.nodeList:
				if node.type=='PRP' and node.getAttribute('reftype')=='E':
					ref = node.getAttribute('ref') 
					if ',' in ref:
						ref=ref.split(',')
					if '%' in ref:
						rsen=ref.split('%')[1]
						rchunk=ref.split('%')[2]
					else:
						rsen=tree.name
						rchunk=ref
					if node.morphroot.strip()==u'तभी':
						resolve(d,node,fileName)

