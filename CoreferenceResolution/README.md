# This Readme is about Coreference system

# # Which includes following submodules

1. Coreference 
    1. Mention Identifier
    2. Entity Anaphora Resolution
    3. Event Anaphora Resolution
    4. Nominal Anaphora Resolution

# # How to run the system
# # # For help 

* python coreference_resolution.py -h

   usage: coreference_resolution.py [-h] [-i INPUT] [-p PATH] [-o OUTPUT]

	optional arguments:
	  -h, --help            show this help message and exit
	  -i INPUT, --input INPUT, -please specify the input file path INPUT
	  -p PATH, --path PATH, -please specify the anaphoraresolver tool`s path PATH
	  -o OUTPUT, --output OUTPUT, -please specify the output file path [optional] OUTPUT 
