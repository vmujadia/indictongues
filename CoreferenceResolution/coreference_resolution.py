# -*- coding: utf-8 -*-
import os
import sys
import imp
import copy
import socket
import random
import pickle
import codecs
import cPickle
import argparse
import collections
import numpy as np

#import ssfAPI_minimal as ssf

from sklearn.feature_extraction import DictVectorizer
from sklearn import tree
from sklearn.externals import joblib
from sklearn import tree
import concrete_anaphora_resolution_main as con_ana
import event_anaphora_resolution_main as eve_ana
import nominal_reference_resolution_main as nom
reload(sys)
sys.setdefaultencoding("utf-8")


def find_match(input_v,nominal_dic):
	for i1 in nominal_dic:
		for j1 in nominal_dic[i1]:
			one=' '.join(value1.lex for value1 in nominal_dic[i1][j1] if value1.lex!=None)
			two=' '.join(value1.lex for value1 in input_v if value1.lex!=None)
			if one==two:
				for k in input_v:
					for k1 in nominal_dic[i1][j1]:
						if k.upper.name == k1.upper.name and k.upper.upper.name == k1.upper.upper.name:
							#print one,two
							return i1,j1
	return False,False


if __name__ == '__main__' :
	parser = argparse.ArgumentParser()
    	parser.add_argument('-i', '--input', '-please specify the input file path')
    	parser.add_argument('-p', '--path', '-please specify the anaphoraresolver tool`s path')
    	parser.add_argument('-o', '--output', '-please specify the output file path [optional]')

    	args = parser.parse_args()

    	if args.input==None:
		print 'For help please use: python anaphora_resolution_main.py -h'
		exit()

    	if args.output!=None:
		f = open(args.output,'w')
		f.write('')
		f.close()
    	if True:
		con_ana_obj=con_ana.concrete_anaphora_resolution_main(args.input,args.path,args.path+'/data/PREFERENCE',args.path+'/model/anaphora.model',args.output)
		d,concrete_dic=con_ana_obj.resolution_run()
		d,event_dic=eve_ana.event_anaphora_resolution_main(d,args.input,args.path)
		d,nominal_dic=nom.nominal_reference_resolution_main(d,args.input,args.path)
		temp1=copy.deepcopy(nominal_dic)
		temp2=copy.deepcopy(concrete_dic)
                #print nominal_dic
		'''
		for i in concrete_dic:
			print 'new ::', i,'\n\t--->',concrete_dic[i]
			for j in concrete_dic[i]:
				print '\t',' '.join(value1.lex+' = '+value1.upper.upper.name for value1 in concrete_dic[i][j] if value1.lex!=None).encode('utf-8'),
			print

		print '\n\n\n=================\n\n\n'
		'''
		#for i in nominal_dic:
		#	for j in nominal_dic[i]:
		#		if nominal_dic[i][j][len(nominal_dic[i][j])-1].type == 'NNP':
		#			print nominal_dic[i][j][len(nominal_dic[i][j])-1].lex
		for i in concrete_dic:
			for j in concrete_dic[i]:
				i1,j1=find_match(concrete_dic[i][j],nominal_dic)
				if i1!=False:
					#print len(temp1[i1]), len(temp1[i1][j1])
					for c in concrete_dic[i]:
						#print ' = '.join(value1.lex for value1 in concrete_dic[i][c])
						temp1[i1][len(temp1[i1][j1])+1]=concrete_dic[i][c]
						temp2[i]=None
						#temp1[i1][len(temp1[i1][j1])+1]=concrete_dic[i][c]
					break

		for i in temp2:
			if temp2[i]!=None:
				temp1[len(temp1)+1]=temp2[i]
				temp2[i]=None

		for i in event_dic:
			if event_dic[i]!=None:
				temp1[len(temp1)+1]=event_dic[i]
				event_dic[i]=None

		fcoref_dic123=temp1
		tcount=0
		icount=0
		if True:
		    temp = []
		    for i in fcoref_dic123:
		        tcount=tcount+1
		        for j in fcoref_dic123[i]:
		            icount=icount+1
		            in_c=0
		            for value in fcoref_dic123[i][j]:
		                in_c=in_c+1
		                for tree in d.nodeList :
		                    if value.upper.upper.name==tree.name:
		                        for chunkNode in tree.nodeList :
		                            if chunkNode.name==value.upper.name:
		                                for node in chunkNode.nodeList:
		                                    if node.name==value.name:
		                                        if in_c==len(fcoref_dic123[i][j]):
		                                            node.addAttribute('pcref','i'+str(icount)+'%1:t'+str(tcount))
		                                            node.addAttribute('pcrefHead',node.lex+':i'+str(icount))
		                                        else:
		                                            node.addAttribute('pcref','i'+str(icount)+'%0:t'+str(tcount))

		for tree in d.nodeList:
            		    print tree.printSSFValue(allFeat=False).encode('utf-8')


		#for i in fcoref_dic123:
		 #   print 'new ::', i
		  #  for j in fcoref_dic123[i]:
		   #     print '\t',' '.join(value1.lex+' = '+value1.upper.upper.name for value1 in fcoref_dic123[i][j] if value1.lex!=None),
		    #    print
		    #print '\n\n'
		'''
		for i in temp1:
			print 'new ::', i
			for j in temp1[i]:
				print '\t',' '.join(value1.lex+' = '+value1.upper.upper.name for value1 in temp1[i][j] if value1.lex!=None).encode('utf-8'),
			print

		print '\n\n\n========\n\n\n'

		for i in temp2:
			print 'new ::', i,'\n\t--->',temp2[i]
		print '\n\n\n========\n\n\n'


		for i in event_dic:
			print 'new ::', i,'\n\t--->',event_dic[i]
		print '\n\n\n========\n\n\n'



		#for i in concrete_dic:
		#	print i, concrete_dic[i]
		for i in concrete_dic:
			print 'new ::', i,'\n\t--->',concrete_dic[i]
			for j in concrete_dic[i]:
				print '\t',' '.join(value1.lex+' = '+value1.upper.upper.name for value1 in concrete_dic[i][j] if value1.lex!=None).encode('utf-8'),
			print

		print '\n\n\n========\n\n\n'

		#for i in event_dic:
		#	print i, event_dic[i]
		for i in event_dic:
			print 'new ::', i,'\n\t--->',event_dic[i]
			for j in nominal_dic[i]:
				print '\t',' '.join(value1.lex+' = '+value1.upper.upper.name for value1 in event_dic[i][j] if value1.lex!=None).encode('utf-8'),
			print

		print '\n\n\n========\n\n\n'
		#for i in nominal_dic:
		#	print i, nominal_dic[i]
		for i in nominal_dic:
			print 'new ::', i,'\n\t--->',nominal_dic[i]
			for j in nominal_dic[i]:
				print '\t',' '.join(value1.lex+' = '+value1.upper.upper.name for value1 in nominal_dic[i][j] if value1.lex!=None).encode('utf-8'),
			print
		print '\n\n'
		'''
