# -*- coding: utf-8 -*-
import sys
sys.path.append('API/')

import imp
import ssfAPI_minimal as ssf
import random
import numpy as np
import cPickle
from sklearn.feature_extraction import DictVectorizer
from sklearn import tree
from sklearn.externals import joblib
from sklearn import tree


sys.path.append('API/eventAPI')
import event_esa as eveesa
import event_isliye as eveliye
import event_itna as eveitna
import event_jab as evejab
import event_jo as evejo
import event_tab as evetab
import event_tabhi as evetabhi
import event_ml as eveml
reload(sys)
sys.setdefaultencoding("utf-8")


def check_found(value,fdic):
	for i in fdic:
		for j in fdic[i]:
			if value==j:
				return True,i
	return False,888

def folderWalk(folderPath):
    import os
    fileList = []
    for dirPath , dirNames , fileNames in os.walk(folderPath) :
        for fileName in fileNames :
            fileList.append(os.path.join(dirPath , fileName))
    return fileList

def update_anaphoraInSSF(ana,ana_ant,number):
        for ii in ana.upper.nodeList:
            if ii==ana:
		if ana.upper.name!=ana_ant.upper.name:
			for ij in ii.nodeList:
				if ij.type=='PRP':
					ij.addAttribute('predictedEvent_ref',str('..%'+ana_ant.upper.name+'%'+ana_ant.name))
		else:
			for ij in ii.nodeList:
				if ij.type=='PRP':
					ij.addAttribute('predictedEvent_ref',str(ana_ant.name))
                break

def event_anaphora_resolution_main(d,fileName,path):
	#if __name__ == '__main__' :
	#	inputPath = sys.argv[1]
	#        fileList = folderWalk(inputPath)
	#	newFileList=fileList
	#        newFileList = []
	#	newFileList.append(inputPath)
	#	#for fileName in newFileList :
	#	#print 'Name ::: ',fileName
	#	d = ssf.Document(fileName)
	if True:
		ana_count=0;s=''
		anaphoraDic={}
		for tree in d.nodeList :
		    for chunkNode in tree.nodeList :
		        for node in chunkNode.nodeList:
				if node.type=='PRP' and node.getAttribute('reftype')=='E' or node.getAttribute('reftype')=='T':
					#print node.text.strip()
					if node.morphroot.strip()==u'ऐसा':
						s=eveesa.resolve(d,node,fileName)
					elif node.morphroot.strip()==u'इसलिये':
						s=eveliye.resolve(d,node,fileName)
					elif node.morphroot.strip()==u'इतना':
						s=eveitna.resolve(d,node,fileName)
					elif node.morphroot.strip()==u'जब':
						s=evejab.resolve(d,node,fileName)
					elif node.morphroot.strip()==u'जो':
						s=evejo.resolve(d,node,fileName)
					elif node.morphroot.strip()==u'तब':
						s=evetab.resolve(d,node,fileName)
					elif node.morphroot.strip()==u'तभी':
						s=evetabhi.resolve(d,node,fileName)
					else:
						s=eveml.resolve(d,node,fileName,path)

					if s!='' and s!=None:
                                                #print s
						chunkNode.isPronounresolved=True
						chunkNode.PrePronounrefstm=s.upper.upper
						chunkNode.PrePronounrefnode=s.upper

					if not chunkNode.isPronounresolved:
						s=eveml.resolve(d,node,fileName,path)

					if s!='' and s!=None:
						chunkNode.isPronounresolved=True
						chunkNode.PrePronounrefstm=s.upper.upper
						chunkNode.PrePronounrefnode=s.upper

					if s!='' and s!=None:
		                            if chunkNode.isPronounresolved:
		                                ana_count=ana_count+1
		                                if chunkNode.PrePronounrefnode in anaphoraDic:
		                                    temp=chunkNode.PrePronounrefnode
		                                    flag=True
		                                    while flag:
		                                        if temp in anaphoraDic:
		                                            temp=anaphoraDic[temp]
		                                        else:
		                                            flag=False
		                                    ##update_anaphoraInSSF(chunkNode, temp, ana_count)
		                                    anaphoraDic[temp]=chunkNode
		                                else:
		                                    ##update_anaphoraInSSF(chunkNode, chunkNode.PrePronounrefnode, ana_count)
		                                    anaphoraDic[chunkNode.PrePronounrefnode]=chunkNode
					    ##else:
					    	##for ij in chunkNode.nodeList:
						##	if ij.type=='PRP':
						##		ij.addAttribute('predictedEvent_ref',str('UNK'))

		#print anaphoraDic
	    	fdic={}
	    	count=0	
	    	for i in anaphoraDic:
			flag,value=check_found(i,fdic)
			if not flag:
				flag,value =check_found(anaphoraDic[i],fdic)
			if not flag:
				fdic[count]=[]
				fdic[count].append(i)
				fdic[count].append(anaphoraDic[i])
				count = count + 1
			else:
				if i not in fdic[value]:
					fdic[value].append(i)
				if anaphoraDic[i] not in fdic[value]:
					fdic[value].append(anaphoraDic[i])

		fi_dic={}
            	count=1
	    	for i in fdic:
			in_c=1
			in_dic={}
			for j in fdic[i]:
				listt=[]
				for k in j.nodeList:
					if k.type!='PSP':
						listt.append(k)
				in_dic[in_c]=listt
				in_c=in_c+1
			fi_dic[count]=in_dic
			count=count+1
	
	    	#for i in fi_dic:
		#	print i, fi_dic[i]

		#for tree in d.nodeList :
		#	print tree.printSSFValue(allFeat=False).encode('utf-8')
		return d,fi_dic
