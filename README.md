# this is ongoing work (we will be updating each and every module very soon)
# IndicTongues

This toolkit serves the basic Indian languages oriented requirements form bottom down level to several top levels.

Currently this tookit contains following modules for several Indic languages like **Hindi**, **Urdu**, **Telugu**, **Tamil**, **Bangla**, **Odia** ,**Gujarati**, **Sindhi**.

1. TextNormalizer/         
2. Tokenizer/
3. POS/                    
4. Morph/               
5. NamedEntityRecognition/ 
6. Chunker/                
7. ShallowParser/          
8. HeadComputation/        
9. VibhaktiComputiation/
10. DependencyParser/       
11. SemanticRoleLabbler/    
12. CoreferenceResolution/  
13. WordEmbeddings/

# How to Install


# How to run


# Developers and Maintainers
1. Vandan Mujadia (vmujadia@gmail.com)
2. Pruthwik Mishra (pruthwikmishra@gmail.com) (LTRC, IIIT-Hyderabad)

# Reviewers
1. Prof. Dipti Misra Sharma (LTRC, IIIT-Hyderabad)
2. Dr. Manish Srivastava (LTRC, IIIT-Hyderabad) 
3. Dr. Radhika Mamidi (LTRC, IIIT-Hyderabad)

