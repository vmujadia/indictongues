# Some Basic coding rules for this repo

1. Never use **_** in any module name, class name
2. Only use Tab for python indentation
3. Always write comments below each and every function/method explaining its functionality and consederations
4. Saperate README and INSTALL file is necessory for each module
